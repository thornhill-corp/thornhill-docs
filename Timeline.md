# Timeline

The timeline allows browsing feeds by date.

# Year timeline

| ![](./images/Timeline_Y1.png) | ![](./images/Timeline_Y2.png) |
|-------------------------------|-------------------------------|

## Year/Month/Day/Hour timeline

| ![](./images/Timeline_YMDH1-1.png) | ![](./images/Timeline_YMDH1-2.png) | ![](./images/Timeline_YMDH1-3.png) | ![](./images/Timeline_YMDH1-4.png) |
|------------------------------------|------------------------------------|------------------------------------|------------------------------------|
|                                    | ![](./images/Timeline_YMDH2-1.png) | ![](./images/Timeline_YMDH2-2.png) | ![](./images/Timeline_YMDH2-3.png) |

## Hour timeline

![](./images/Timeline_H.png)

## Primary operations' year timeline

![](./images/Timeline_Y_PrimaryOperations.png)