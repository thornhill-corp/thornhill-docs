# Operating system

An operating system was created for the Machine.

Among other things, it features :

- a terminal ;
- a code editor ;
- an output panel ;
- an image viewer.

## Lockscreen

It is protected by lockscreen requiring a password *and* positive facial recognition by the Machine.

![](./images/Lockscreen.png)

## Terminal

The terminal is the way [admin](Reference.md#admin) controls the Machine.

A progress bar appears when running a task.

| ![](./images/Terminal-1.png) | ![](./images/Terminal-2.png) |
|------------------------------|------------------------------|

## Code editor

![](./images/Code_Editor.png)